using Interfaces_HomeWork.Domain;
using Interfaces_HomeWork.Interfaces;
using Interfaces_HomeWork.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace XUnitTest
{
   public class Test
   {
      [Fact]
      public void AddAccount_Correctly_Callback()
      {
         Account testAccount = GetAlexandr();
         List<Account> accounts = new List<Account>();

         Mock<IRepository<Account>> moq = new Mock<IRepository<Account>>();
         moq.Setup(m => m.Add(testAccount)).Callback((Account c) => accounts.Add(c));

         AccountService accountService = new AccountService(moq.Object);
         accountService.AddAccount(testAccount);
         Assert.Single(accounts);
         Assert.Equal(testAccount, accounts.First());
      }
      [Fact]
      public void AddAccount_Correctly_Verify()
      {
         Account testAccount = GetAlexandr();

         Mock<IRepository<Account>> moq = new Mock<IRepository<Account>>();
         moq.Setup(m => m.Add(testAccount));

         AccountService accountService = new AccountService(moq.Object);
         accountService.AddAccount(testAccount);
         moq.Verify(v => v.Add(testAccount), Times.Once);
      }
      [Fact]
      public void AddAccount_AlreadyExistsException()
      {
         Account testAccount = GetIvan();
         Mock<IRepository<Account>> moq = new Mock<IRepository<Account>>();
         moq.Setup(a => a.Add(testAccount));
         moq.Setup(a => a.GetAll()).Returns(GetAll());

         AccountService accountService = new AccountService(moq.Object);
         Exception exception = Assert.Throws<Exception>(() => accountService.AddAccount(testAccount));
         Assert.Equal("Account already exists", exception.Message);
      }
      [Fact]
      public void AddAccount_EmptyNameException()
      {
         Account testAccount = GetIvan();
         testAccount.FirstName = "";
         Mock<IRepository<Account>> moq = new Mock<IRepository<Account>>();
         moq.Setup(a => a.Add(testAccount));
         moq.Setup(a => a.GetAll()).Returns(GetAll());

         AccountService accountService = new AccountService(moq.Object);
         Exception exception = Assert.Throws<Exception>(() => accountService.AddAccount(testAccount));
         Assert.Equal("First name must be not empty", exception.Message);
      }
      [Fact]
      public void AddAccount_Younger18yoException()
      {
         Account testAccount = GetSamwell();
         Mock<IRepository<Account>> moq = new Mock<IRepository<Account>>();
         moq.Setup(a => a.Add(testAccount));
         moq.Setup(a => a.GetAll()).Returns(GetAll());

         AccountService accountService = new AccountService(moq.Object);
         Exception exception = Assert.Throws<Exception>(() => accountService.AddAccount(testAccount));
         Assert.Equal("You're to young for this (must be 18 y.o. or older)", exception.Message);
      }
      [Fact]
      public void DeleteAccount_Correctly_Callback()
      {
         Account testAccount = GetAlexandr();
         List<Account> accounts = new List<Account>() { testAccount };

         Mock<IRepository<Account>> moq = new Mock<IRepository<Account>>();
         moq.Setup(m => m.GetAll()).Returns(accounts);
         moq.Setup(m => m.Delete(testAccount)).Callback<Account>(c => accounts.Remove(c));

         AccountService accountService = new AccountService(moq.Object);
         Assert.Single(accounts);
         accountService.DeleteAccount(testAccount);
         Assert.Empty(accounts);
      }
      [Fact]
      public void DeleteAccount_Correctly_Verify()
      {
         Account testAccount = GetAlexandr();
         List<Account> accounts = new List<Account>() { testAccount };

         Mock<IRepository<Account>> moq = new Mock<IRepository<Account>>();
         moq.Setup(m => m.GetAll()).Returns(accounts);
         moq.Setup(m => m.Delete(testAccount));

         AccountService accountService = new AccountService(moq.Object);
         accountService.DeleteAccount(testAccount);
         moq.Verify(v => v.Delete(testAccount), Times.Once);
      }
      private IEnumerable<Account> GetAll()
         => new List<Account>()
         {
            GetIvan()
         };
      private Account GetAlexandr()
         => new Account 
         { 
            FirstName = "Alexandr",
            LastName = "Alexandrov",
            BirthDate = new DateTime(year: 2002, month: 1, day: 1, hour: 00, minute: 00, second: 01) 
         };
      private Account GetIvan()
         => new Account
         {
            FirstName = "Ivan",
            LastName = "Ivanov",
            BirthDate = new DateTime(year: 2000, month: 1, day: 1, hour: 00, minute: 00, second: 01)
         };
      private Account GetSamwell()
         => new Account
         {
            FirstName = "Samwell",
            LastName = "Tarly",
            BirthDate = new DateTime(year: 2010, month: 1, day: 1, hour: 00, minute: 00, second: 01)
         };

   }
}
