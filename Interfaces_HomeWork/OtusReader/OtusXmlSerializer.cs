﻿using ExtendedXmlSerializer;
using Interfaces_HomeWork.Interfaces;
using System.IO;

namespace Interfaces_HomeWork.OtusReader
{
   public class OtusXmlSerializer<T> : ISerializer<T>
   {
      private readonly IExtendedXmlSerializer serializer;
      public OtusXmlSerializer(IExtendedXmlSerializer serializer) => this.serializer = serializer;
      public T Deserialize<T>(Stream stream) => serializer.Deserialize<T>(stream);
      public string Serialize<T>(T item) => serializer.Serialize(item);
   }
}
