﻿using Interfaces_HomeWork.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Interfaces_HomeWork.OtusReader
{
   public class OtusStreamReader<T> : IEnumerable<T>
   {
      private readonly T[] collection;

      public OtusStreamReader(ISerializer<T> serializer, Stream stream)
      {
         this.collection = serializer.Deserialize<T[]>(stream);
      }

      IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

      public IEnumerator<T> GetEnumerator()
      {
         for (int i = 0; i < this.collection.Length; i++)
         {
            yield return collection[i];
         }
      }
   }
}
