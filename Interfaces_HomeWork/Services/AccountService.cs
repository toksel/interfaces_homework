﻿using Interfaces_HomeWork.Domain;
using Interfaces_HomeWork.Interfaces;
using System;
using System.Linq;

namespace Interfaces_HomeWork.Services
{
   public class AccountService : IAccountService
   {
      private readonly IRepository<Account> repository;

      public AccountService(IRepository<Account> repository)
         => this.repository = repository;

      public void AddAccount(Account account)
      {
         if (string.IsNullOrEmpty(account.FirstName)) { throw new Exception("First name must be not empty"); }
         if (new DateTime((DateTime.Now - account.BirthDate).Ticks).Year - 1 < 18) { throw new Exception("You're to young for this (must be 18 y.o. or older)"); }
         if (repository.GetAll().Where(w => w == account).Any()) { throw new Exception("Account already exists"); }
         repository.Add(account);
      }

      public void DeleteAccount(Account account)
      {
         if (!repository.GetAll().Where(w => w == account).Any()) { throw new Exception("Account does not exist"); }
         repository.Delete(account);
      }
   }
}
