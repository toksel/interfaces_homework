﻿using Interfaces_HomeWork.Interfaces;
using System.IO;

namespace Interfaces_HomeWork.FileProvider
{
   public class FileProvider<T> : IDataProvider<T>
   {
      private readonly string filePath;
      private readonly ISerializer<T> serializer;

      public FileProvider(ISerializer<T> serializer, string filePath)
      {
         this.serializer = serializer;
         this.filePath = File.Exists(filePath) ? filePath : throw new FileNotFoundException();
      }

      public Stream GetStream()
      {
         Stream stream;
         using (FileStream fileStream = new FileStream(this.filePath, FileMode.Open))
         {
            byte[] buffer = new byte[fileStream.Length];
            fileStream.Read(buffer, 0, buffer.Length);
            stream = new MemoryStream(buffer);
         }
         return stream;
      }

      public void StoreData(T[] data)
      {
         using (FileStream stream = new FileStream(this.filePath, FileMode.Truncate))
         {
            using (StreamWriter writer = new StreamWriter(stream))
            {
               writer.Write(serializer.Serialize(data));
            }
         }
      }
   }
}
