﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Interfaces_HomeWork.Interfaces
{
   public interface IDataProvider<T>
   {
      Stream GetStream();
      void StoreData(T[] data);
   }
}
