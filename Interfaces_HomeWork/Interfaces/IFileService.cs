﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Interfaces_HomeWork.Interfaces
{
   public interface IFileService
   {
      Stream GetFileStream(string path);
      void SaveFile(string path);
   }
}
