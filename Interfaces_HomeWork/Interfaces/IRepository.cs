﻿using System;
using System.Collections.Generic;

namespace Interfaces_HomeWork.Interfaces
{
   public interface IRepository<T>
   {
      IEnumerable<T> GetAll();
      T GetOne(Func<T, bool> predicate);
      void Add(T item);
      void Delete(T item);
   }
}
