﻿using System.Collections.Generic;

namespace Interfaces_HomeWork.Interfaces
{
   public interface IAlgorythm<T>
   {
      IEnumerable<T> ReverseSort();
   }
}
