﻿using System.Collections.Generic;

namespace Interfaces_HomeWork.Interfaces
{
   public interface ISorter<T>
   {
      IEnumerable<T> Sort<T>(IEnumerable<T> nonSortedItems);
   }
}
