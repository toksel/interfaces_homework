﻿using Interfaces_HomeWork.Domain;

namespace Interfaces_HomeWork.Interfaces
{
   public interface IAccountService
   {
      void AddAccount(Account account);
      void DeleteAccount(Account account);
   }
}
