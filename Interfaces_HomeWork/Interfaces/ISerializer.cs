﻿using System.IO;

namespace Interfaces_HomeWork.Interfaces
{
   public interface ISerializer<T>
   {
      string Serialize<T>(T item);
      T Deserialize<T>(Stream stram);
   }
}
