﻿using Interfaces_HomeWork.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Interfaces_HomeWork
{
   internal static class Extensions
   {
      internal static string Done(this bool isHomeworkDone) => isHomeworkDone ? "has done the homework" : "hasn't done the homework yet";
      internal static IEnumerable<T> Add<T> (this IEnumerable<T> collection, T item)
      {
         List<T> extendedCollection = collection.ToList();
         extendedCollection.Add(item);
         return extendedCollection;
      }
      internal static Account[] ToArray(this IEnumerable<Account> collection)
      {
         Account[] accounts = new Account[collection.Count()];
         int i = 0;
         foreach (var account in collection)
         {
            accounts[i++] = account;
         }
         return accounts;
      }
   }
}
