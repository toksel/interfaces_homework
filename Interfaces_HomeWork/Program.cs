﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using Interfaces_HomeWork.Domain;
using Interfaces_HomeWork.FileProvider;
using Interfaces_HomeWork.OtusReader;
using Interfaces_HomeWork.Services;
using System;
using System.IO;
using System.Text;

namespace Interfaces_HomeWork
{
   class Program
   {
      static void Main(string[] args)
      {
         #region(Person)

         Console.WriteLine("Person region");

         IExtendedXmlSerializer personSerializer = new ConfigurationContainer().EnableImplicitTypingFromAll<Person>().Create();
         OtusXmlSerializer<Person> otusPersonXmlSerializer = new OtusXmlSerializer<Person> (personSerializer);

         OtusStreamReader<Person> otusReader;
         using (StreamReader reader = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), @"Data\People.xml"), Encoding.UTF8))
         {
            otusReader = new OtusStreamReader<Person>(otusPersonXmlSerializer, reader.BaseStream);
         }

         Console.WriteLine("Non-sorted collection");
         foreach (var person in otusReader)

         {
            Console.WriteLine(person);
         }
         Console.WriteLine(Environment.NewLine);

         Console.WriteLine("Sorted collection (by Age)");
         PersonSorter sorted = new PersonSorter(otusReader);
         foreach (var item in sorted.People)
         {
            Console.WriteLine(item);
         }
         Console.WriteLine(Environment.NewLine);


         Console.WriteLine("Sorted collection (by Age descending)");
         foreach (var item in sorted.ReverseSort())
         {
            Console.WriteLine(item);
         }
         Console.WriteLine(Environment.NewLine);

         #endregion

         #region(Account)

         Console.WriteLine("Account region");

         IExtendedXmlSerializer accountSerializer = new ConfigurationContainer().EnableImplicitTypingFromAll<Account>().Create();
         OtusXmlSerializer<Account> otusAccountSerializer = new OtusXmlSerializer<Account>(accountSerializer);
         FileProvider<Account> fileProvider = new FileProvider<Account>(otusAccountSerializer, Path.Combine(Directory.GetCurrentDirectory(), @"Data\Accounts.xml"));

         AccountRepository repository = new AccountRepository(otusAccountSerializer, fileProvider);
         AccountService service = new AccountService(repository);

         Account newAccount = new Account() { FirstName = "Alexandr", LastName = "Alexandrov", BirthDate = new DateTime(year: 2002, month: 1, day: 1, hour: 00, minute: 00, second: 01) };

         service.AddAccount(newAccount);

         foreach (var item in repository.GetAll())
         {
            Console.WriteLine(item);
         }

         var accountEnumerator = repository.GetAll().GetEnumerator();
         while (accountEnumerator.MoveNext())
         {
            Console.WriteLine($"Enumerator usage example - {accountEnumerator.Current}");
         }

         service.DeleteAccount(newAccount);

         Console.WriteLine($"\nAfter deleting account {newAccount}");
         foreach (var account in repository.GetAll())
         {
            Console.WriteLine(account);
         }

         Console.WriteLine("\n");
         Console.WriteLine(repository.GetOne(o => o.FirstName == "Semen"));

         #endregion

         Console.ReadKey();
      }
   }
}
