﻿using System;

namespace Interfaces_HomeWork.Domain
{
   public class Person : IComparable
   {
      public string Name { get; set; }
      public string Surname { get; set; }
      public int Age { get; set; }
      public bool IsHomeWorkDone { get; set; }

      public override string ToString()
      {
         return $"{Name} {Surname} ({Age} y.o.) {IsHomeWorkDone.Done()}";
      }
      public int CompareTo(object otherPerson)
         => otherPerson is Person person ?
         this.Name.CompareTo(person.Name) * 100 + this.Surname.CompareTo(person.Surname) * 10 + this.Age.CompareTo(person.Age) :
         throw new Exception($"Unable to compare type Person with {otherPerson.GetType().Name}");
      public int GetAge() => Age;
   }
}
