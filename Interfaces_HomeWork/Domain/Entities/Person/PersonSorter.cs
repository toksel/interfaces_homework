﻿using Interfaces_HomeWork.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Interfaces_HomeWork.Domain
{
   public class PersonSorter : ISorter<Person>, IAlgorythm<Person>
   {
      public IEnumerable<Person> People { get; }
      public PersonSorter(IEnumerable<Person> nonSortedCollection)
      {
         People = Sort(nonSortedCollection);
      }
      public IEnumerable<Person> Sort(IEnumerable<Person> nonSortedCollection)
         => nonSortedCollection.OrderBy(o => o.Age);

      public IEnumerable<Person> ReverseSort()
         => People.OrderByDescending(o => o.Age);

      public IEnumerable<Person> Sort<Person>(IEnumerable<Person> nonSortedCollection)
         => Sort(nonSortedCollection);
   }
}
