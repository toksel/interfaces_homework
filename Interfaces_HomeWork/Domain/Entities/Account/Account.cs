﻿using System;

namespace Interfaces_HomeWork.Domain
{
   public class Account
   {
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public DateTime BirthDate { get; set; }
      public override string ToString() => $"{FirstName} {LastName} {BirthDate}";

      public static bool operator ==(Account a, Account b)
         => a.FirstName == b.FirstName &&
         a.LastName == b.LastName &&
         a.BirthDate == b.BirthDate;

      public static bool operator !=(Account a, Account b)
         => a.FirstName != b.FirstName ||
         a.LastName != b.LastName ||
         a.BirthDate != b.BirthDate;

      public override bool Equals(object obj)
         => obj != null &&
         FirstName.Equals(((Account)obj).FirstName) &&
         LastName.Equals(((Account)obj).LastName) &&
         BirthDate.Equals(((Account)obj).BirthDate);

      public override int GetHashCode()
         => $"{FirstName} {LastName} {BirthDate}".GetHashCode();
   }
}
