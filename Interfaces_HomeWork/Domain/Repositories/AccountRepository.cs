﻿using Interfaces_HomeWork.Interfaces;
using Interfaces_HomeWork.OtusReader;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Interfaces_HomeWork.Domain
{
   public class AccountRepository : IRepository<Account>
   {
      private readonly ISerializer<Account> serializer;
      private readonly IDataProvider<Account> dataProvider;

      public AccountRepository(ISerializer<Account> serializer, IDataProvider<Account> dataProvider)
      {
         this.serializer = serializer;
         this.dataProvider = dataProvider;
      }
      public void Add(Account item)
         => this.dataProvider.StoreData(GetAll().Add(item).ToArray());

      public void Delete(Account item)
         => this.dataProvider.StoreData(GetAll().Where(w => w != item).ToArray());

      public IEnumerable<Account> GetAll()
         => new OtusStreamReader<Account>(this.serializer, this.dataProvider.GetStream());

      public Account GetOne(Func<Account, bool> predicate)
         => GetAll().Where(predicate).FirstOrDefault() ??
         throw new Exception("Accound does not extst");
   }
}
